clc;
clear;
syms x1 y1 x2 y2 lambda1 lambda2
%control variables x1, y1, x2, y2
%constants xc1, yc1, xc2, yc2, b0, h0, l0,
b0 = 0.065;
h0 = 0.123;
l0 = 0.683;
xc1 = 0;
yc1 = 0;
xc2 = sqrt(l0^2-h0^2);
yc2 = 0;

%x1 = 0.05;
%y1 = 0.05;
%x2 = 0.01;
%y2 = 0.005;
xb = 0.7281;
yb = 0.0207;
zb = 0.0801;

eq1 = (xb-(x1+xc1))^2 + (yb-(y2+yc2))^2 + zb^2 - l0^2;
eq2 = (xb-(x2+xc2))^2 + (yb-(y2+yc2))^2 +(zb-h0)^2 -b0^2;
eq3 = 1 - lambda1*(2*(xb-(x1+xc1))) - lambda2*(2*(xb-(x2+xc2)));
eq4 = lambda1*(2*(yb-(y1+yc1))) + lambda2*(2*(yb-(y2+yc2)));
eq5 = lambda1*2*zb + lambda2*(2*(zb-h0));

system = [eq1; eq2; eq3; eq4; eq5];
jacobian_matrix = jacobian(system, [x1, y1, x2, y2, lambda1, lambda2])
max = 1000;
xk_current = [0; 0; 0; 0; 0; 0];
delta = 0.000001;
epsilon = 0.000001;

for k = 1:max
    jacobian_matrix_numeric = double(subs(jacobian_matrix, {x1, y1, x2, y2, lambda1, lambda2}, {xk_current(1), xk_current(2), xk_current(3), xk_current(4), xk_current(5), xk_current(6)}));
    system_numeric = double(subs(system, {x1, y1, x2, y2, lambda1, lambda2}, {xk_current(1), xk_current(2), xk_current(3), xk_current(4), xk_current(5), xk_current(6)}))
    %xk_next = xk_current - inv(transpose(jacobian_matrix_numeric)*jacobian_matrix_numeric)*transpose(jacobian_matrix_numeric)*system_numeric
    xk_next = xk_current - pinv(jacobian_matrix_numeric)*system_numeric;
    %xk_next = xk_current - system_numeric*pinv(jacobian_matrix_numeric);
    err = norm(xk_next-xk_current);
    relerr = err/(norm(xk_next)+eps);
    xk_current = xk_next;
    if (err < delta || relerr < delta || norm(system_numeric) < epsilon)
        break;
    end
    %norm(system_numeric)
    %k
end
xk_current
%double(asin((xk_current(2)-(y2+yc2))/b0))
%double(asin((xk_current(3)-h0)/b0))