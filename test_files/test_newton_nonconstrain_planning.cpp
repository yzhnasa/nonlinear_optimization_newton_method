#include <iostream>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include "solver.h"

double kinematicEquationJointTip_1(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return xt - xr - (a0 / b0) * (xb - xr);
}

double kinematicEquationJointTip_2(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return yt - yr - (a0 / b0) * (yb - yr);
}

double kinematicEquationJointTip_3(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return zt - h0 - (a0 / b0) * (zb - h0);
}

double kinematicEquationJointTip_4(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb - xr) * (x - xt) + (yb - yr) * (y - yt) + (zb - h0) * (z - zt);
}

double kinematicEquationJointTip_5(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb - xr) * (y - yt) - (x - xt) * (yb - yr);
}

double kinematicEquationJointTip_6(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (x - xb) * (x - xb) + (y - yb) * (y - yb) + (z - zb) * (z - zb) - (a0 - b0) * (a0 - b0) - c0 * c0;
}

double kinematicEquationJointTip_inv_1(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return xt - xr - (a0 / b0) * (xb - xr);
}

double kinematicEquationJointTip_inv_2(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return yt - yr - (a0 / b0) * (yb - yr);
}

double kinematicEquationJointTip_inv_3(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return zt - h0 - (a0 / b0) * (zb - h0);
}

double kinematicEquationJointTip_inv_4(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb - xr) * (x - xt) + (yb - yr) * (y - yt) + (zb - h0) * (z - zt);
}

double kinematicEquationJointTip_inv_5(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb - xr) * (y - yt) - (x - xt) * (yb - yr);
}

double kinematicEquationJointTip_inv_6(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (x - xb) * (x - xb) + (y - yb) * (y - yb) + (z - zb) * (z - zb) - (a0 - b0) * (a0 - b0) - c0 * c0;
}

double kinematicEquationJointTip_inv_7(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (x-xr)*(x-xr) + (y-yr)*(y-yr) + (z-h0)*(z-h0) - c0*c0 - a0*a0;
}

double kinematicEquationJointTip_inv_8(const Eigen::VectorXd &joint_cable_attaching_end_coordinate, const Eigen::VectorXd &tip_coordinate) {
    double xr = joint_cable_attaching_end_coordinate(0);
    double yr = joint_cable_attaching_end_coordinate(1);

    double xb = joint_cable_attaching_end_coordinate(2);
    double yb = joint_cable_attaching_end_coordinate(3);
    double zb = joint_cable_attaching_end_coordinate(4);

    double xt = joint_cable_attaching_end_coordinate(5);
    double yt = joint_cable_attaching_end_coordinate(6);
    double zt = joint_cable_attaching_end_coordinate(7);

    double x = tip_coordinate(0);
    double y = tip_coordinate(1);
    double z = tip_coordinate(2);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xt-xr)*(y-yt) - (x-xt)*(yt-yr);
}

int main(){
    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    boost::container::vector<KinematicEquationPtr> kinematic_equations_inv;
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_1);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_2);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_3);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_4);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_5);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_6);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_7);
    kinematic_equations_inv.push_back((KinematicEquationPtr)kinematicEquationJointTip_inv_8);

    UnconstrainedSolver solver_inv(kinematic_equations_inv);

    Eigen::VectorXd planned_tip_coordinate_inv(3);
    planned_tip_coordinate_inv(0) = 8.82977;
    planned_tip_coordinate_inv(1) = 0.47967;
    planned_tip_coordinate_inv(2) = 0.48119;
    Eigen::VectorXd current_control_value_inv(8);
    current_control_value_inv(0) = xc2;       //xr yr
    current_control_value_inv(1) = 0;
    current_control_value_inv(2) = xc2+d0+b0; //xb yb zb
    current_control_value_inv(3) = 0;
    current_control_value_inv(4) = h0;
    current_control_value_inv(5) = xc2+d0+a0; //xt yt zt
    current_control_value_inv(6) = 0;
    current_control_value_inv(7) = h0;

    Eigen::VectorXd result_inverse_kinematic_inv(8);
    result_inverse_kinematic_inv = solver_inv.solveInverseKinematic(current_control_value_inv, planned_tip_coordinate_inv);
    std::cout << "inverse kinematic result" << std::endl;
    std::cout << result_inverse_kinematic_inv << std::endl;

    Eigen::VectorXd current_tip_coordinate_inv(3);
    current_tip_coordinate_inv(0) = xc2+d0+a0;
    current_tip_coordinate_inv(1) = 0;
    current_tip_coordinate_inv(2) = h0-c0;

    Eigen::VectorXd new_joint_cable_attaching_end_coordinate(8);
    new_joint_cable_attaching_end_coordinate(0) = 7.51553;
    new_joint_cable_attaching_end_coordinate(1) = 1.27369;
    new_joint_cable_attaching_end_coordinate(2) = 8.13196;
    new_joint_cable_attaching_end_coordinate(3) = 0.901261;
    new_joint_cable_attaching_end_coordinate(4) = 1.09963;
    new_joint_cable_attaching_end_coordinate(5) = 8.88884;
    new_joint_cable_attaching_end_coordinate(6) = 0.443983;
    new_joint_cable_attaching_end_coordinate(7) = 0.976405;

    Eigen::VectorXd result_forword_kinematic_inv(3);
    result_forword_kinematic_inv = solver_inv.solveForwardKinematic(new_joint_cable_attaching_end_coordinate, current_tip_coordinate_inv);
    std::cout << "forword kinematic result_inv" << std::endl;
    std::cout << result_forword_kinematic_inv << std::endl;

    Eigen::VectorXd planned_tip_coordinate(6);
    planned_tip_coordinate(0) = 8.88884;
    planned_tip_coordinate(1) = 0.443983;
    planned_tip_coordinate(2) = 0.976405;
    planned_tip_coordinate(3) = 8.82978;
    planned_tip_coordinate(4) = 0.479666;
    planned_tip_coordinate(5) = 0.481199;

    Eigen::VectorXd current_control_value(5);
    current_control_value(0) = xc2;
    current_control_value(1) = 0;
    current_control_value(2) = xc2+d0+b0;
    current_control_value(3) = 0;
    current_control_value(4) = h0;

    UnconstrainedSolver solver;
    solver.setKinematicEquation((KinematicEquationPtr)kinematicEquationJointTip_1);
    solver.setKinematicEquation((KinematicEquationPtr)kinematicEquationJointTip_2);
    solver.setKinematicEquation((KinematicEquationPtr)kinematicEquationJointTip_3);
    solver.setKinematicEquation((KinematicEquationPtr)kinematicEquationJointTip_4);
    solver.setKinematicEquation((KinematicEquationPtr)kinematicEquationJointTip_5);
    solver.setKinematicEquation((KinematicEquationPtr)kinematicEquationJointTip_6);

    Eigen::VectorXd result_inverse_kinematic(5);
    result_inverse_kinematic = solver.solveInverseKinematic(current_control_value, planned_tip_coordinate);
    std::cout << "inverse kinematic result" << std::endl;
    std::cout << result_inverse_kinematic << std::endl;

    Eigen::VectorXd current_tip_coordinate(6);
    current_tip_coordinate(0) = xc2+d0+a0;
    current_tip_coordinate(1) = 0;
    current_tip_coordinate(2) = h0;
    current_tip_coordinate(3) = xc2+d0+a0;
    current_tip_coordinate(4) = 0;
    current_tip_coordinate(5) = h0-c0;

    Eigen::VectorXd new_joint_cable_attaching_coordinate(5);
    new_joint_cable_attaching_coordinate(0) = 7.51554;
    new_joint_cable_attaching_coordinate(1) = 1.27371;
    new_joint_cable_attaching_coordinate(2) = 8.13197;
    new_joint_cable_attaching_coordinate(3) = 0.901268;
    new_joint_cable_attaching_coordinate(4) = 1.09964;
    Eigen::VectorXd result_forword_kinematic(5);
    result_forword_kinematic = solver.solveForwardKinematic(new_joint_cable_attaching_coordinate, current_tip_coordinate);
    std::cout << "forword kinematic result" << std::endl;
    std::cout << result_forword_kinematic << std::endl;

    return 0;
}
