#include <iostream>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include "solver.h"


double kinematic_equation1(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb-(x1+xc1))*(xb-(x1+xc1)) + (yb-(y2+yc2))*(yb-(y2+yc2)) + zb*zb - l0*l0;
}

double kinematic_equation2(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb-(x2+xc2))*(xb-(x2+xc2)) + (yb-(y2+yc2))*(yb-(y2+yc2)) + (zb-h0)*(zb-h0) - b0*b0;
}

double constrained_kinematic_equation1(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &lambda){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return 1-lambda1*(2*(xb-(x1+xc1))) - lambda2*(2*(xb-(x2+xc2)));
}

double constrained_kinematic_equation2(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &lambda){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda1*(2*(yb-(y1+yc1))) + lambda2*(2*(yb-(y2+yc2)));
}

double constrained_kinematic_equation3(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &lambda){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda1*(2*zb) + lambda2*(2*(zb-h0));
}

int main(){
    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    boost::container::vector<KinematicEquationPtr> kinematic_equations;
    kinematic_equations.push_back((KinematicEquationPtr)kinematic_equation1);
    kinematic_equations.push_back((KinematicEquationPtr)kinematic_equation2);

    boost::container::vector<LagrangianAuxiliaryEquationPtr> constrained_kinematic_equations;
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrained_kinematic_equation1);
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrained_kinematic_equation2);
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrained_kinematic_equation3);

    ConstrainedSolver solver(kinematic_equations, constrained_kinematic_equations);

    Eigen::VectorXd planned_tip_coordinate(3);
    planned_tip_coordinate(0) = 0.7281;
    planned_tip_coordinate(1) = 0.0207;
    planned_tip_coordinate(2) = 0.0801;

    Eigen::VectorXd current_control_value(4);
    current_control_value(0) = 0;
    current_control_value(1) = 0;
    current_control_value(2) = 0;
    current_control_value(3) = 0;

    Eigen::VectorXd multiplier(2);
    multiplier(0) = 0;
    multiplier(1) = 0;

    Eigen::VectorXd result_inverse_kinematic(4);
    result_inverse_kinematic = solver.solveInverseKinematic(current_control_value, planned_tip_coordinate, multiplier);
    std::cout << "inverse kinematic result" << std::endl;
    std::cout << result_inverse_kinematic << std::endl;

    Eigen::VectorXd current_tip_coordinate(3);
    current_tip_coordinate(0) = xc2+b0;
    current_tip_coordinate(1) = 0;
    current_tip_coordinate(2) = h0;

    Eigen::VectorXd planned_control_value(4);
    planned_control_value(0) = 0.05;
    planned_control_value(1) = -0.0063;
    planned_control_value(2) = 0.0096;
    planned_control_value(3) = 0.0352;

    Eigen::VectorXd result_forword_kinematic(3);
    result_forword_kinematic = solver.solveForwardKinematic(planned_control_value, current_tip_coordinate, multiplier);
    std::cout << "forword kinematic result" << std::endl;
    std::cout << result_forword_kinematic << std::endl;

    return 0;
}
