#include <iostream>
#include <cmath>
#include <memory>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include "solver.h"

class KEquation1 : public KinematicEquation {
    public:
        KEquation1() : KinematicEquation() {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &state) {
            double p_x = state(0);
            double theta1 = control_value(0);
            double theta2 = control_value(1);
            double theta3 = control_value(2);
            double theta4 = control_value(3);
            double theta5 = control_value(4);

            double a2 = 0.5;
            double d4 = 0.5;
            double d6 = 0.5;
            return a2*std::cos(theta1)*std::cos(theta2) + d4*std::cos(theta1)*std::sin(theta2+theta3) + d6*(std::cos(theta1)*(std::cos(theta2+theta3)*std::cos(theta4)*std::sin(theta5)+std::sin(theta2+theta3)*std::cos(theta5))+std::sin(theta1)*std::sin(theta4)*std::sin(theta5)) - p_x;
        }
};

class KEquation2 : public KinematicEquation {
    public:
        KEquation2() : KinematicEquation() {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &state) {
            double p_y = state(1);
            double theta1 = control_value(0);
            double theta2 = control_value(1);
            double theta3 = control_value(2);
            double theta4 = control_value(3);
            double theta5 = control_value(4);

            double a2 = 0.5;
            double d4 = 0.5;
            double d6 = 0.5;
            return a2*std::cos(theta1)*std::cos(theta2) + d4*std::cos(theta1)*std::sin(theta2+theta3) + d6*(std::cos(theta1)*(std::cos(theta2+theta3)*std::cos(theta4)*std::sin(theta5)+std::sin(theta2+theta3)*std::cos(theta5))-std::cos(theta1)*std::sin(theta4)*std::sin(theta5)) - p_y;
        }
};

class KEquation3 : public KinematicEquation {
    public:
        KEquation3() : KinematicEquation() {}
        const double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &state) {
            double p_z = state(2);
            double theta1 = control_value(0);
            double theta2 = control_value(1);
            double theta3 = control_value(2);
            double theta4 = control_value(3);
            double theta5 = control_value(4);

            double a2 = 0.5;
            double d4 = 0.5;
            double d6 = 0.5;
            return a2*std::sin(theta2) - d4*std::cos(theta2+theta3) + d6*(std::sin(theta2+theta3)*std::cos(theta4)*std::sin(theta5)-std::cos(theta2+theta3)*std::cos(theta5)) - p_z;
        }
};

int main(int argc, char **argv) {
    UnconstrainedSolver solver;
    solver.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<KEquation1>()));
    solver.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<KEquation2>()));
    solver.setKinematicEquation((std::shared_ptr<KinematicEquation>)(std::make_shared<KEquation3>()));

    Eigen::VectorXd planned_control_value(6);
    Eigen::VectorXd current_state(3);

    planned_control_value(0) = 3.26275;
    planned_control_value(1) = -14.7808;
    planned_control_value(2) = 12.9806;
    planned_control_value(3) = 0.995172;
    planned_control_value(4) = 5.94274;
    planned_control_value(5) = 0;

    current_state(0) = 0;
    current_state(1) = 0;
    current_state(2) = 0;

    Eigen::VectorXd new_state(3);
    new_state = solver.solveForwardKinematic(planned_control_value, current_state);
    std::cout << "Forward Kinematic" << std::endl;
    std::cout << new_state << std::endl;

    Eigen::VectorXd current_control_value(6);
    Eigen::VectorXd planned_state(3);

    current_control_value(0) = 0;
    current_control_value(1) = 0;
    current_control_value(2) = 0;
    current_control_value(3) = 0;
    current_control_value(4) = 0;
    current_control_value(5) = 0;

    planned_state(0) = 1.23318;
    planned_state(1) = 1.07723;
    planned_state(2) = -0.0905;

    Eigen::VectorXd new_control_value(6);
    new_control_value = solver.solveInverseKinematic(current_control_value, planned_state);
    std::cout << "Inverse Kinematic" << std::endl;
    std::cout << new_control_value << std::endl;

    return 0;
}
