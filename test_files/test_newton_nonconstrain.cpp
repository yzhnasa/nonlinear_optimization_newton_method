#include <iostream>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include "solver.h"


double kinematicEquationJointTip_1(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return xt - xr - (a0 / b0) * (xb - xr);
}

double kinematicEquationJointTip_2(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return yt - yr - (a0 / b0) * (yb - yr);
}

double kinematicEquationJointTip_3(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return zt - h0 - (a0 / b0) * (zb - h0);
}

double kinematicEquationJointTip_4(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb - xr) * (x - xt) + (yb - yr) * (y - yt) + (zb - h0) * (z - zt);
}

double kinematicEquationJointTip_5(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb - xr) * (y - yt) - (x - xt) * (yb - yr);
}

double kinematicEquationJointTip_6(const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &end_and_tip_coordinate) {
    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double xt = end_and_tip_coordinate(0);
    double yt = end_and_tip_coordinate(1);
    double zt = end_and_tip_coordinate(2);

    double x = end_and_tip_coordinate(3);
    double y = end_and_tip_coordinate(4);
    double z = end_and_tip_coordinate(5);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (x - xb) * (x - xb) + (y - yb) * (y - yb) + (z - zb) * (z - zb) - (a0 - b0) * (a0 - b0) - c0 * c0;
}

int main(){
    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    boost::container::vector<KinematicEquationPtr> kinematic_equations;
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationJointTip_1);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationJointTip_2);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationJointTip_3);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationJointTip_4);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationJointTip_5);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationJointTip_6);

    UnconstrainedSolver solver(kinematic_equations);

    Eigen::VectorXd planned_tip_coordinate(6);
    planned_tip_coordinate(0) = 7.3975;
    planned_tip_coordinate(1) = 0.816839;
    planned_tip_coordinate(2) = 1.44236;
    planned_tip_coordinate(3) = 7.5133;
    planned_tip_coordinate(4) = 0.840484;
    planned_tip_coordinate(5) = 0.774635;

    Eigen::VectorXd current_control_value(5);
    current_control_value(0) = xc2;
    current_control_value(1) = 0;
    current_control_value(2) = xc2+d0+b0;
    current_control_value(3) = 0;
    current_control_value(4) = h0;

    Eigen::VectorXd result_inverse_kinematic(5);
    result_inverse_kinematic = solver.solveInverseKinematic(current_control_value, planned_tip_coordinate);
    std::cout << "inverse kinematic result" << std::endl;
    std::cout << result_inverse_kinematic << std::endl;

    Eigen::VectorXd current_tip_coordinate(6);
    current_tip_coordinate(0) = xc2+d0+a0;
    current_tip_coordinate(1) = 0;
    current_tip_coordinate(2) = h0;
    current_tip_coordinate(3) = xc2+d0+a0;
    current_tip_coordinate(4) = 0;
    current_tip_coordinate(5) = h0-c0;

    Eigen::VectorXd new_joint_cable_attaching_coordinate(5);
    new_joint_cable_attaching_coordinate(0) = 6.05593;
    new_joint_cable_attaching_coordinate(1) = 0.542905;
    new_joint_cable_attaching_coordinate(2) = 6.65812;
    new_joint_cable_attaching_coordinate(3) = 0.665866;
    new_joint_cable_attaching_coordinate(4) = 1.30879;
    Eigen::VectorXd result_forword_kinematic(5);
    result_forword_kinematic = solver.solveForwardKinematic(new_joint_cable_attaching_coordinate, current_tip_coordinate);
    std::cout << "forword kinematic result" << std::endl;
    std::cout << result_forword_kinematic << std::endl;

    return 0;
}
