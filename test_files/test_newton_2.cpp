#include <iostream>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include "solver.h"


double kinematicEquationControlValueToCableAttachingPoint_1(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb-(x1+xc1))*(xb-(x1+xc1)) + (yb-(y1+yc1))*(yb-(y1+yc1)) + zb*zb - l0*l0;
}

double kinematicEquationControlValueToCableAttachingPoint_2(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xb-xr)*(xb-xr) + (yb-yr)*(yb-yr) + (zb-h0)*(zb-h0) - b0*b0;
}

double kinematicEquationControlValueToCableAttachingPoint_3(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xr-(x2+xc2))*(xr-(x2+xc2)) + (yr-(y2+yc2))*(yr-(y2+yc2)) - d0*d0;
}

double kinematicEquationControlValueToCableAttachingPoint_4(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return (xr-(x2+xc2))*(yb-yr) - (xb-xr)*(yr-(y2+yc2));
}

double constrainedKinematicEquationControlValueToCableAttachingPoint_1(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);
    double lambda3 = lambda(2);
    double lambda4 = lambda(3);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return 1 - lambda1*(2*(xb-(x1+xc1))) - lambda2*(2*(xb-xr)) + lambda4*(yr-(y2+yc2));
}

double constrainedKinematicEquationControlValueToCableAttachingPoint_2(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);
    double lambda3 = lambda(2);
    double lambda4 = lambda(3);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda1*(2*(yb-(y1+yc1))) + lambda2*(2*(yb-yr)) + lambda4*(xr-(x2+xc2));
}

double constrainedKinematicEquationControlValueToCableAttachingPoint_3(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);
    double lambda3 = lambda(2);
    double lambda4 = lambda(3);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda1*(2*zb) + lambda2*(2*(zb-h0));
}

double constrainedKinematicEquationControlValueToCableAttachingPoint_4(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);
    double lambda3 = lambda(2);
    double lambda4 = lambda(3);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda2*(2*(xb-xr)) - lambda3*(2*(xr-(x2+xc2))) - lambda4*((yb-yr)-(yr-(y2+yc2)));
}

double constrainedKinematicEquationControlValueToCableAttachingPoint_5(const Eigen::VectorXd &driving_values, const Eigen::VectorXd &joint_and_cable_attaching_coordinate, const Eigen::VectorXd &lambda) {
    double x1 = driving_values(0);
    double y1 = driving_values(1);
    double x2 = driving_values(2);
    double y2 = driving_values(3);

    double xr = joint_and_cable_attaching_coordinate(0);
    double yr = joint_and_cable_attaching_coordinate(1);

    double xb = joint_and_cable_attaching_coordinate(2);
    double yb = joint_and_cable_attaching_coordinate(3);
    double zb = joint_and_cable_attaching_coordinate(4);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);
    double lambda3 = lambda(2);
    double lambda4 = lambda(3);

    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda2*(2*(yb-yr)) - lambda3*(2*(yr-(y2+yc2))) + lambda4*((xr-(x2+xc2))+(xb-xr));
}

int main(){
    double a0 = 1.62;
    double b0 = 0.72717;
    double c0 = 0.5;
    double d0 = 0.15;
    double h0 = 1.2;
    double l0 = 7.58;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    boost::container::vector<KinematicEquationPtr> kinematic_equations;
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationControlValueToCableAttachingPoint_1);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationControlValueToCableAttachingPoint_2);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationControlValueToCableAttachingPoint_3);
    kinematic_equations.push_back((KinematicEquationPtr)kinematicEquationControlValueToCableAttachingPoint_4);

    boost::container::vector<LagrangianAuxiliaryEquationPtr> constrained_kinematic_equations;
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrainedKinematicEquationControlValueToCableAttachingPoint_1);
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrainedKinematicEquationControlValueToCableAttachingPoint_2);
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrainedKinematicEquationControlValueToCableAttachingPoint_3);
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrainedKinematicEquationControlValueToCableAttachingPoint_4);
    constrained_kinematic_equations.push_back((LagrangianAuxiliaryEquationPtr)constrainedKinematicEquationControlValueToCableAttachingPoint_5);

    ConstrainedSolver solver(kinematic_equations, constrained_kinematic_equations);

    Eigen::VectorXd planned_tip_coordinate(5);
    planned_tip_coordinate(0) = 7.51554;
    planned_tip_coordinate(1) = 1.27371;
    planned_tip_coordinate(2) = 8.13197;
    planned_tip_coordinate(3) = 0.901268;
    planned_tip_coordinate(4) = 1.09964;

    Eigen::VectorXd current_control_value(4);
    current_control_value(0) = 0;
    current_control_value(1) = 0;
    current_control_value(2) = 0;
    current_control_value(3) = 0;

    Eigen::VectorXd multiplier(4);
    multiplier(0) = 0;
    multiplier(1) = 0;
    multiplier(2) = 0;
    multiplier(3) = 0;

    Eigen::VectorXd result_inverse_kinematic(4);
    result_inverse_kinematic = solver.solveInverseKinematic(current_control_value, planned_tip_coordinate, multiplier);
    std::cout << "inverse kinematic result" << std::endl;
    std::cout << result_inverse_kinematic << std::endl;

    Eigen::VectorXd current_tip_coordinate(5);
    current_tip_coordinate(0) = xc2;
    current_tip_coordinate(1) = 0;
    current_tip_coordinate(2) = xc2+d0+b0;
    current_tip_coordinate(3) = 0;
    current_tip_coordinate(4) = h0;

    std::cout << "current tip coordinate" << std::endl;
    std::cout << current_tip_coordinate << std::endl;

    Eigen::VectorXd planned_control_value(4);
    planned_control_value(0) = 1.83959;
    planned_control_value(1) = -3.17956;
    planned_control_value(2) = 0.159515;
    planned_control_value(3) = 1.19614;

    multiplier(0) = 0;
    multiplier(1) = 0;
    multiplier(2) = 0;
    multiplier(3) = 0;

    Eigen::VectorXd result_forword_kinematic(5);
    result_forword_kinematic = solver.solveForwardKinematic(planned_control_value, current_tip_coordinate, multiplier);
    std::cout << "forword kinematic result" << std::endl;
    std::cout << result_forword_kinematic << std::endl;

    return 0;
}
