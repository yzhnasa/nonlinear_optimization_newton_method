#include <iostream>
//#include <boost/container/vector.hpp>
//#include <boost/numeric/ublas/matrix.hpp>
#include <eigen3/Eigen/Dense>
//#include <vector>
#include <cmath>
#include "jacobian.h"


double kinematic_equation1(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    //std::cout << x*x - 2*x - y +0.5 << " " << std::endl;
    return (xb-(x1+xc1))*(xb-(x1+xc1)) + (yb-(y2+yc2))*(yb-(y2+yc2)) + zb*zb - l0*l0;
}

double kinematic_equation2(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;
    //return x*x*x + 5*x*x + x*sin(5*x*x);
    return (xb-(x2+xc2))*(xb-(x2+xc2)) + (yb-(y2+yc2))*(yb-(y2+yc2)) + (zb-h0)*(zb-h0) - b0*b0;
}

double constrained_kinematic_equation1(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &lambda){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return 1-lambda1*(2*(xb-(x1+xc1))) - lambda2*(2*(xb-(x2+xc2)));
}

double constrained_kinematic_equation2(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &lambda){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda1*(2*(yb-(y1+yc1))) + lambda2*(2*(yb-(y2+yc2)));
}

double constrained_kinematic_equation3(const Eigen::VectorXd &control_variables, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &lambda){
    double x1 = control_variables(0);
    double y1 = control_variables(1);
    double x2 = control_variables(2);
    double y2 = control_variables(3);

    double xb = tip_coordinate(0);
    double yb = tip_coordinate(1);
    double zb = tip_coordinate(2);

    double lambda1 = lambda(0);
    double lambda2 = lambda(1);

    double b0 = 0.065;
    double h0 = 0.123;
    double l0 = 0.683;
    double xc1 = 0;
    double yc1 = 0;
    double xc2 = std::sqrt(l0*l0 - h0*h0);
    double yc2 = 0;

    return lambda1*(2*zb) + lambda2*(2*(zb-h0));
}
int main(){
    boost::container::vector<KinematicEquationPtr> kinematic_equations;
    //std::vector<equation_ptr> *equations = new std::vector<equation_ptr>();
    kinematic_equations.push_back((KinematicEquationPtr)kinematic_equation1);
    kinematic_equations.push_back((KinematicEquationPtr)kinematic_equation2);

    boost::container::vector<ConstrainedKinematicEquationPtr> constrained_kinematic_equations;
    constrained_kinematic_equations.push_back((ConstrainedKinematicEquationPtr)constrained_kinematic_equation1);
    constrained_kinematic_equations.push_back((ConstrainedKinematicEquationPtr)constrained_kinematic_equation2);
    constrained_kinematic_equations.push_back((ConstrainedKinematicEquationPtr)constrained_kinematic_equation3);

    Jacobian *jacobian = new Jacobian(kinematic_equations, constrained_kinematic_equations);

    Eigen::VectorXd planned_tip_coordinate(3);
    planned_tip_coordinate(0) = 0.7281;
    planned_tip_coordinate(1) = 0.0207;
    planned_tip_coordinate(2) = 0.0801;

    Eigen::VectorXd current_control_value(4);
    current_control_value(0) = 0;
    current_control_value(1) = 0;
    current_control_value(2) = 0;
    current_control_value(3) = 0;

    Eigen::VectorXd multiplier(2);
    multiplier(0) = 0;
    multiplier(1) = 0;

    Eigen::MatrixXd jacobianMatrix = *jacobian->calculateConstrainedInverseKinematicJacobianMatrix(current_control_value, planned_tip_coordinate, multiplier);
    for(unsigned int i = 0; i < 5; i++){
        for(unsigned int j = 0; j < 6; j++)
            std::cout << jacobianMatrix(i, j) << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}
