#ifndef __EQUATION_H__
#define __EQUATION_H__

#include <memory>
#include <limits>
#include <boost/container/vector.hpp>
#include <eigen3/Eigen/Dense>


typedef const double (*EquationPtr)(const Eigen::VectorXd&);
typedef const double (*KinematicEquationPtr)(const Eigen::VectorXd&, const Eigen::VectorXd&);
typedef const double (*LagrangianAuxiliaryEquationPtr)(const Eigen::VectorXd&, const Eigen::VectorXd&, const Eigen::VectorXd&);
typedef const double (*KinematicErrorEquationPtr)(const Eigen::VectorXd&, const Eigen::VectorXd&, const Eigen::VectorXd&);
typedef const double (*FunctionPtr)(double);

class MaxIteration {
    public:
        static constexpr unsigned int MAX_100   =   100;
        static constexpr unsigned int MAX_1000  =  1000;
        static constexpr unsigned int MAX_10000 = 10000;
};

class Precision {
    public:
        static constexpr double MAX_DIGITS_6  =          0.000001;
        static constexpr double MAX_DIGITS_9  =       0.000000001;
        static constexpr double MAX_DIGITS_12 =    0.000000000001;
        static constexpr double MAX_DIGITS_15 = 0.000000000000001;
};

class InitialStepSize {
    public:
        static constexpr double SIZE_DIGITS_0 = 1.0;
        static constexpr double SIZE_DIGITS_1 = 0.1;
        static constexpr double SIZE_DIGITS_2 = 0.01;
};

class Equation {
    protected:
        Eigen::VectorXd constants_;
    public:
        Equation() {}
        Equation(Eigen::VectorXd &constants) : constants_(constants) {}
        const virtual double operator()(const Eigen::VectorXd &value) = 0;
};

class KinematicEquation : protected Equation {
    protected:
        const double operator()(const Eigen::VectorXd &value){return 0;};
    public:
        KinematicEquation() {}
        KinematicEquation(Eigen::VectorXd &constants) : Equation(constants) {}
        const virtual double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &state) = 0;
};

class LagrangianAuxiliaryEquation : protected Equation {
    protected:
        const double operator()(const Eigen::VectorXd &value){return 0;};
    public:
        LagrangianAuxiliaryEquation() {}
        LagrangianAuxiliaryEquation(Eigen::VectorXd &constants) : Equation(constants) {}
        const virtual double operator()(const Eigen::VectorXd &control_value, const Eigen::VectorXd &state, const Eigen::VectorXd &multiplier) = 0;
};

#endif /* __EQUATION_H__ */
