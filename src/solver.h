#ifndef __SOLVER_H__
#define __SOLVER_H__

#include <memory>
#include <boost/container/vector.hpp>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/SVD>
#include "jacobian.h"

class Solver {
    private:
        boost::container::vector<EquationPtr> equations_ptrs_;
        boost::container::vector<std::shared_ptr<Equation>> equations_;
    protected:
        const unsigned int max_iteration_;
        const double delta_;
        const double epsilon_;

        Eigen::MatrixXd pseudoInverse(const Eigen::MatrixXd &matrix);
        virtual Eigen::VectorXd calculateSystemVector(const Eigen::VectorXd &value);
        virtual Eigen::VectorXd newtonMethod(const Eigen::VectorXd &inital_value);
    public:
        Solver();
        Solver(const boost::container::vector<EquationPtr> &equations_ptrs);
        Solver(const boost::container::vector<std::shared_ptr<Equation>> &equations);
        Solver(unsigned int max_iteration, double delta, double epsilon);
        Solver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<EquationPtr> &equations_ptrs);
        Solver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<Equation>> &equations);
        void setEquation(const EquationPtr equation_ptr);
        void setEquation(std::shared_ptr<Equation> equation);
        void setEquations(const boost::container::vector<EquationPtr> &equation_ptrs);
        void setEquations(const boost::container::vector<std::shared_ptr<Equation>> &equation);
        virtual Eigen::VectorXd solveEquation(const Eigen::VectorXd &inital_value);
        ~Solver();
};

class UnconstrainedSolver : protected Solver {
    protected:
        boost::container::vector<KinematicEquationPtr> kinematic_equations_ptrs_;
        boost::container::vector<std::shared_ptr<KinematicEquation>> kinematic_equations_;
        virtual Eigen::VectorXd calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate);
        virtual Eigen::VectorXd newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate);
    public:
        UnconstrainedSolver();
        UnconstrainedSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        UnconstrainedSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        UnconstrainedSolver(unsigned int max_iteration, double delta, double epsilon);
        UnconstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        UnconstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        void setKinematicEquation(const KinematicEquationPtr kinematic_equation_ptr);
        void setKinematicEquation(std::shared_ptr<KinematicEquation> kinematic_equation);
        void setKinematicEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs);
        void setKinematicEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations);
        virtual Eigen::VectorXd solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate);
        virtual Eigen::VectorXd solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate);
        ~UnconstrainedSolver();
};

class ConstrainedSolver : public UnconstrainedSolver {
    protected:
        boost::container::vector<LagrangianAuxiliaryEquationPtr> lagrangian_auxiliary_equations_ptrs_;
        boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> lagrangian_auxiliary_equations_;
        virtual Eigen::VectorXd calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier);
        virtual Eigen::VectorXd newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier);
    public:
        ConstrainedSolver();
        ConstrainedSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        ConstrainedSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        ConstrainedSolver(unsigned int max_iteration, double delta, double epsilon);
        ConstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        ConstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        void setLagrangianAuxiliaryEquation(const LagrangianAuxiliaryEquationPtr lagrangian_auxiliary_equation_ptr);
        void setLagrangianAuxiliaryEquation(std::shared_ptr<LagrangianAuxiliaryEquation> lagrangian_auxiliary_equation);
        void setLagrangianAuxiliaryEquations(const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        void setLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        void setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs);
        void setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations);
        virtual Eigen::VectorXd solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier);
        virtual Eigen::VectorXd solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier);
        ~ConstrainedSolver();
};

#endif /* __SOLVER_H__ */
