#include "solver.h"

Solver::Solver()
    : max_iteration_(MaxIteration::MAX_1000),
      delta_(Precision::MAX_DIGITS_12),
      epsilon_(Precision::MAX_DIGITS_12) {}

Solver::Solver(const boost::container::vector<EquationPtr> &equations_ptrs)
    : max_iteration_(MaxIteration::MAX_1000),
      delta_(Precision::MAX_DIGITS_12),
      epsilon_(Precision::MAX_DIGITS_12) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

Solver::Solver(const boost::container::vector<std::shared_ptr<Equation>> &equations)
    : max_iteration_(MaxIteration::MAX_1000),
      delta_(Precision::MAX_DIGITS_12),
      epsilon_(Precision::MAX_DIGITS_12) {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

Solver::Solver(unsigned int max_iteration, double delta, double epsilon)
    : max_iteration_(max_iteration),
      delta_(delta),
      epsilon_(epsilon) {}

Solver::Solver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<EquationPtr> &equations_ptrs)
    : max_iteration_(max_iteration),
      delta_(delta),
      epsilon_(epsilon) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

Solver::Solver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<Equation>> &equations)
    : max_iteration_(max_iteration),
      delta_(delta),
      epsilon_(epsilon) {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

Eigen::MatrixXd Solver::pseudoInverse(const Eigen::MatrixXd &matrix) {
    Eigen::MatrixXd inverse_matrix(matrix.rows(), matrix.cols());
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(matrix, Eigen::ComputeThinU | Eigen::ComputeThinV);
    double epsilon = std::numeric_limits<double>::epsilon();
    double tolerance = epsilon * std::max(matrix.cols(), matrix.rows()) *svd.singularValues().array().abs()(0);
    inverse_matrix = svd.matrixV() * (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
    return inverse_matrix;
}

Eigen::VectorXd Solver::calculateSystemVector(const Eigen::VectorXd &value){
    Eigen::VectorXd system_vector(equations_ptrs_.size());
    if(!equations_ptrs_.empty()){
        for(unsigned int i = 0; i < equations_ptrs_.size(); i++){
            system_vector.coeffRef(i) = (*equations_ptrs_.at(i))(value);
        }
    }else{
        system_vector.resize(equations_.size());
        for(unsigned int i = 0; i < equations_.size(); i++){
            system_vector.coeffRef(i) = (*equations_.at(i))(value);
        }
    }
    return system_vector;
}

Eigen::VectorXd Solver::newtonMethod(const Eigen::VectorXd &initial_value) {
    Eigen::VectorXd xk_current;
    Eigen::VectorXd xk_next;
    Eigen::VectorXd error_vector;
    double error;
    double relative_error;
    Jacobian jacobian;

    if(!equations_ptrs_.empty()){
        jacobian.setEquations(equations_ptrs_);
    }else{
        jacobian.setEquations(equations_);
    }

    Eigen::MatrixXd jacobian_matrix;
    Eigen::MatrixXd inverse_jacobian_matrix;
    Eigen::VectorXd system_vector;
    xk_current.resize(initial_value.size());
    xk_current = initial_value;
    for(unsigned int i = 0; i < max_iteration_; i++){
        jacobian_matrix = jacobian.calculateJacobianMatrix(xk_current);
        inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
        system_vector = calculateSystemVector(xk_current);
        xk_next = xk_current - inverse_jacobian_matrix*system_vector;
        error_vector = xk_next - xk_current;
        error = error_vector.norm();
        relative_error = error/(xk_next.norm());
        xk_current = xk_next;
        if(error < delta_ || relative_error < delta_ || system_vector.norm() < epsilon_)
            break;
    }

    Eigen::VectorXd result(xk_current.size());
    result = xk_current;
    return result;
}

void Solver::setEquation(const EquationPtr equation_ptr) {
    equations_ptrs_.push_back(equation_ptr);
}

void Solver::setEquation(std::shared_ptr<Equation> equation) {
    equations_.push_back(equation);
}

void Solver::setEquations(const boost::container::vector<EquationPtr> &equations_ptrs) {
    for(unsigned int i = 0; i < equations_ptrs.size(); i++){
        equations_ptrs_.push_back(equations_ptrs.at(i));
    }
}

void Solver::setEquations(const boost::container::vector<std::shared_ptr<Equation>> &equations) {
    for(unsigned int i = 0; i < equations.size(); i++){
        equations_.push_back(equations.at(i));
    }
}

Eigen::VectorXd Solver::solveEquation(const Eigen::VectorXd &initial_value) {
    return newtonMethod(initial_value);
}

Solver::~Solver() {}

UnconstrainedSolver::UnconstrainedSolver() : Solver() {}

UnconstrainedSolver::UnconstrainedSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs) : Solver() {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

UnconstrainedSolver::UnconstrainedSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations) : Solver() {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

UnconstrainedSolver::UnconstrainedSolver(unsigned int max_iteration, double delta, double epsilon)
    : Solver(max_iteration, delta, epsilon) {}

UnconstrainedSolver::UnconstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs)
    : Solver(max_iteration, delta, epsilon) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

UnconstrainedSolver::UnconstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations)
    : Solver(max_iteration, delta, epsilon) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

Eigen::VectorXd UnconstrainedSolver::calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate) {
    Eigen::VectorXd system_vector(kinematic_equations_ptrs_.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_ptrs_.at(i))(control_value, tip_coordinate);
        }
    }else{
        system_vector.resize(kinematic_equations_.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_.at(i))(control_value, tip_coordinate);
        }
    }
    return system_vector;
}

Eigen::VectorXd UnconstrainedSolver::newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate) {
    Eigen::VectorXd xk_current;
    Eigen::VectorXd xk_next;
    Eigen::VectorXd error_vector;
    double error;
    double relative_error;
    UnconstrainedJacobian jacobian;
    if(!kinematic_equations_ptrs_.empty()){
        jacobian.setKinematicEquations(kinematic_equations_ptrs_);
    }else{
        jacobian.setKinematicEquations(kinematic_equations_);
    }

    Eigen::MatrixXd jacobian_matrix;
    Eigen::MatrixXd inverse_jacobian_matrix;
    Eigen::VectorXd system_vector;

    if(forward){
        xk_current.resize(tip_coordinate.size());
        xk_current = tip_coordinate;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateForwardKinematicJacobianMatrix(control_value, xk_current);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(control_value, xk_current);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < delta_ || relative_error < delta_ || system_vector.norm() < epsilon_)
                break;
        }
    }else{
        xk_current.resize(control_value.size());
        xk_current = control_value;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateInverseKinematicJacobianMatrix(xk_current, tip_coordinate);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(xk_current, tip_coordinate);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < delta_ || relative_error < delta_ || system_vector.norm() < epsilon_)
                break;
        }
    }

    Eigen::VectorXd result(xk_current.size());
    result = xk_current;
    return result;
}

void UnconstrainedSolver::setKinematicEquation(const KinematicEquationPtr kinematic_equation_ptr) {
    kinematic_equations_ptrs_.push_back(kinematic_equation_ptr);
}

void UnconstrainedSolver::setKinematicEquation(std::shared_ptr<KinematicEquation> kinematic_equation) {
    kinematic_equations_.push_back(kinematic_equation);
}

void UnconstrainedSolver::setKinematicEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
}

void UnconstrainedSolver::setKinematicEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
}

Eigen::VectorXd UnconstrainedSolver::solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate) {
    return newtonMethod(true, planned_control_value, current_tip_coordinate);
}

Eigen::VectorXd UnconstrainedSolver::solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate) {
    return newtonMethod(false, current_control_value, planned_tip_coordinate);
}

UnconstrainedSolver::~UnconstrainedSolver() {}

ConstrainedSolver::ConstrainedSolver() : UnconstrainedSolver() {}

ConstrainedSolver::ConstrainedSolver(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs)
    : UnconstrainedSolver(kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

ConstrainedSolver::ConstrainedSolver(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations)
    : UnconstrainedSolver(kinematic_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

ConstrainedSolver::ConstrainedSolver(unsigned int max_iteration, double delta, double epsilon) : UnconstrainedSolver(max_iteration, delta, epsilon) {}

ConstrainedSolver::ConstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs)
    : UnconstrainedSolver(max_iteration, delta, epsilon, kinematic_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

ConstrainedSolver::ConstrainedSolver(unsigned int max_iteration, double delta, double epsilon, const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations)
    : UnconstrainedSolver(max_iteration, delta, epsilon, kinematic_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

Eigen::VectorXd ConstrainedSolver::calculateSystemVector(const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier) {
    Eigen::VectorXd system_vector(kinematic_equations_ptrs_.size()+lagrangian_auxiliary_equations_ptrs_.size());
    if(!kinematic_equations_ptrs_.empty()){
        for(unsigned int i = 0; i < kinematic_equations_ptrs_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_ptrs_.at(i))(control_value, tip_coordinate);
        }
        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs_.size(); i++){
            system_vector.coeffRef(kinematic_equations_ptrs_.size()+i) = (*lagrangian_auxiliary_equations_ptrs_.at(i))(control_value, tip_coordinate, multiplier);
        }
    }else{
        system_vector.resize(kinematic_equations_.size()+lagrangian_auxiliary_equations_.size());
        for(unsigned int i = 0; i < kinematic_equations_.size(); i++){
            system_vector.coeffRef(i) = (*kinematic_equations_.at(i))(control_value, tip_coordinate);
        }
        for(unsigned int i = 0; i < lagrangian_auxiliary_equations_.size(); i++){
            system_vector.coeffRef(kinematic_equations_.size()+i) = (*lagrangian_auxiliary_equations_.at(i))(control_value, tip_coordinate, multiplier);
        }
    }
    return system_vector;
}

Eigen::VectorXd ConstrainedSolver::newtonMethod(bool forward, const Eigen::VectorXd &control_value, const Eigen::VectorXd &tip_coordinate, const Eigen::VectorXd &multiplier) {
    Eigen::VectorXd xk_current;
    Eigen::VectorXd xk_current_value;
    Eigen::VectorXd xk_current_multiplier;
    Eigen::VectorXd xk_next;
    Eigen::VectorXd error_vector;
    double error;
    double relative_error;
    ConstrainedJacobian jacobian;

    if(!kinematic_equations_ptrs_.empty()){
        jacobian.setKinematicAndLagrangianAuxiliaryEquations(kinematic_equations_ptrs_, lagrangian_auxiliary_equations_ptrs_);
    }else{
        jacobian.setKinematicAndLagrangianAuxiliaryEquations(kinematic_equations_, lagrangian_auxiliary_equations_);
    }

    Eigen::MatrixXd jacobian_matrix;
    Eigen::MatrixXd inverse_jacobian_matrix;
    Eigen::VectorXd system_vector;
    if(forward){
        xk_current.resize(tip_coordinate.size()+multiplier.size());
        xk_current << tip_coordinate, multiplier;
        xk_current_value = tip_coordinate;
        xk_current_multiplier = multiplier;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateForwardKinematicJacobianMatrix(control_value, xk_current_value, xk_current_multiplier);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(control_value, xk_current_value, xk_current_multiplier);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < delta_ || relative_error < delta_ || system_vector.norm() < epsilon_)
                break;
            xk_current_value = xk_current.segment(0, xk_current_value.size());
            xk_current_multiplier = xk_current.segment(xk_current_value.size(), xk_current_multiplier.size());
        }
    }else{
        xk_current.resize(control_value.size()+multiplier.size());
        xk_current << control_value, multiplier;
        xk_current_value = control_value;
        xk_current_multiplier = multiplier;
        for(unsigned int i = 0; i < max_iteration_; i++){
            jacobian_matrix = jacobian.calculateInverseKinematicJacobianMatrix(xk_current_value, tip_coordinate, xk_current_multiplier);
            inverse_jacobian_matrix = pseudoInverse(jacobian_matrix);
            system_vector = calculateSystemVector(xk_current_value, tip_coordinate, xk_current_multiplier);
            xk_next = xk_current - inverse_jacobian_matrix*system_vector;
            error_vector = xk_next - xk_current;
            error = error_vector.norm();
            relative_error = error/(xk_next.norm());
            xk_current = xk_next;
            if(error < delta_ || relative_error < delta_ || system_vector.norm() < epsilon_)
                break;
            xk_current_value = xk_current.segment(0, xk_current_value.size());
            xk_current_multiplier = xk_current.segment(xk_current_value.size(), xk_current_multiplier.size());
        }
    }

    Eigen::VectorXd result(xk_current_value.size());
    result = xk_current_value;
    return result;
}

void ConstrainedSolver::setLagrangianAuxiliaryEquation(const LagrangianAuxiliaryEquationPtr lagrangian_auxiliary_equation_ptr) {
    lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equation_ptr);
}

void ConstrainedSolver::setLagrangianAuxiliaryEquation(std::shared_ptr<LagrangianAuxiliaryEquation> lagrangian_auxiliary_equation) {
    lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equation);
}

void ConstrainedSolver::setLagrangianAuxiliaryEquations(const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

void ConstrainedSolver::setLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations) {
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

void ConstrainedSolver::setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<KinematicEquationPtr> &kinematic_equations_ptrs, const boost::container::vector<LagrangianAuxiliaryEquationPtr> &lagrangian_auxiliary_equations_ptrs) {
    for(unsigned int i = 0; i < kinematic_equations_ptrs.size(); i++){
        kinematic_equations_ptrs_.push_back(kinematic_equations_ptrs.at(i));
    }
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations_ptrs.size(); i++){
        lagrangian_auxiliary_equations_ptrs_.push_back(lagrangian_auxiliary_equations_ptrs.at(i));
    }
}

void ConstrainedSolver::setKinematicAndLagrangianAuxiliaryEquations(const boost::container::vector<std::shared_ptr<KinematicEquation>> &kinematic_equations, const boost::container::vector<std::shared_ptr<LagrangianAuxiliaryEquation>> &lagrangian_auxiliary_equations) {
    for(unsigned int i = 0; i < kinematic_equations.size(); i++){
        kinematic_equations_.push_back(kinematic_equations.at(i));
    }
    for(unsigned int i = 0; i < lagrangian_auxiliary_equations.size(); i++){
        lagrangian_auxiliary_equations_.push_back(lagrangian_auxiliary_equations.at(i));
    }
}

Eigen::VectorXd ConstrainedSolver::solveForwardKinematic(const Eigen::VectorXd &planned_control_value, const Eigen::VectorXd &current_tip_coordinate, const Eigen::VectorXd &multiplier) {
    return newtonMethod(true, planned_control_value, current_tip_coordinate, multiplier);
}

Eigen::VectorXd ConstrainedSolver::solveInverseKinematic(const Eigen::VectorXd &current_control_value, const Eigen::VectorXd &planned_tip_coordinate, const Eigen::VectorXd &multiplier) {
    return newtonMethod(false, current_control_value, planned_tip_coordinate, multiplier);
}

ConstrainedSolver::~ConstrainedSolver() {}
